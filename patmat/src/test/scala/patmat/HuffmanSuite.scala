package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
    val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
    val t3 = Fork(Leaf('c',1), Leaf('d',1), List('c','d'), 2)
    val t4 = Fork(
      Fork(Leaf('a', 1), Leaf('b', 2), List('a', 'b'), 3),
      Leaf('c', 4),
      List('a', 'b', 'c'),
      7)

    val table1: CodeTable = List(('a', List(0)), ('b', List(1)))
    val table2: CodeTable = List(('a', List(0, 0)), ('b', List(0, 1)), ('d', List(1)))

  }

  test("weight of a leaf") {
    assert(weight(Leaf('a', 3)) === 3)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("chars of a leaf") {
    assert(chars(Leaf('a', 1)) === List('a'))
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }

  test("times") {
    assert(times(Nil) === Nil)
    assert(times("a".toList) === List(('a', 1)))
    assert(times("aaa".toList) === List(('a', 3)))
    assert(times("bla".toList).toSet === Set(('b', 1), ('l', 1), ('a', 1))) //because sets aren't ordered
    assert(times("blabla".toList).toSet === Set(('b', 2), ('l', 2), ('a', 2)))
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("makeOrderedLeafList - empty") {
    assert(makeOrderedLeafList(Nil) === Nil)
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }

  test("singleton") {
    new TestTrees {
      assert(singleton(Nil) === false)
      assert(singleton(List(Leaf('e', 1))) === true)
      assert(singleton(List(t1)) === true)
      assert(singleton(List(t1, t2)) === false)
    }
  }

  test("combine - no combining") {
    new TestTrees {
      assert(combine(Nil) === Nil)
      assert(combine(List(t1)) === List(t1))
    }
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("combine - Fork") {
    new TestTrees {
      val l = Leaf('e', 1)
      assert(combine(List(t1, l)) === List(Fork(t1, l, List('a', 'b', 'e'), 6)))
      assert(combine(List(t1, t3)) === List(Fork(t1, t3, List('a', 'b', 'c', 'd'), 7)))
    }
  }

  test("combine - ordering stays correct") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 2))
    assert(combine(leaflist) === List(Leaf('x', 2), Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3)))
  }

  test("until - just one tree") {
    new TestTrees {
      assert(until(singleton, combine)(List(t1)) === List(t1))
    }
  }

  test("until") {
    new TestTrees {
      val leaflist = List(Leaf('a', 1), Leaf('b', 2), Leaf('c', 4))
      assert(until(singleton, combine)(leaflist) === List(t4))
    }
  }

  test("createCodeTree") {
    new TestTrees {
      assert(createCodeTree("a".toList) === Leaf('a', 1))
      assert(createCodeTree("aa".toList) === Leaf('a', 2))
      assert(createCodeTree("aabbb".toList) === t1)
      assert(createCodeTree("abbba".toList) === t1)
      assert(createCodeTree("babab".toList) === t1)
      assert(createCodeTree("cbacbcc".toList) === t4)
    }
  }

  test("decode") {
    new TestTrees {
      assert(decode(t1, List(0)) === "a".toList)
      assert(decode(t1, List(1)) === "b".toList)
      assert(decode(t1, List(1, 0, 0)) === "baa".toList)
      assert(decode(t2, List(0, 0)) === "a".toList)
      assert(decode(t2, List(0, 0, 1)) === "ad".toList)
      assert(decode(t2, List(0, 0, 1, 0, 1)) === "adb".toList)
    }
  }

  test("decodedSecret") {
    assert(decodedSecret === "huffmanestcool".toList)
  }

  test("encode") {
    new TestTrees {
      assert(encode(t1)("a".toList) === List(0))
      assert(encode(t1)("b".toList) === List(1))
      assert(encode(t1)("baa".toList) === List(1, 0, 0))
      assert(encode(t2)("a".toList) === List(0, 0))
      assert(encode(t2)("ad".toList) === List(0, 0, 1))
      assert(encode(t2)("adb".toList) === List(0, 0, 1, 0, 1))
      assert(encode(frenchCode)("huffmanestcool".toList) === secret)
    }
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("decode+encode on a French sentence") {
    val sentence = "jettelescrevettesdanslapoubelle".toList
    assert(decode(frenchCode, encode(frenchCode)(sentence)) === sentence)
  }

  test("codeBits") {
    new TestTrees {
      assert(codeBits(table1)('a') === List(0))
      assert(codeBits(table1)('b') === List(1))
      assert(codeBits(table2)('b') === List(0, 1))
    }
  }

  test("convert") {
    new TestTrees {
      assert(convert(t1) === table1)
      assert(convert(t2) === table2)
    }
  }

  test("quickencode") {
    new TestTrees {
      assert(quickEncode(t1)("a".toList) === List(0))
      assert(quickEncode(t1)("b".toList) === List(1))
      assert(quickEncode(t1)("baa".toList) === List(1, 0, 0))
      assert(quickEncode(t2)("a".toList) === List(0, 0))
      assert(quickEncode(t2)("ad".toList) === List(0, 0, 1))
      assert(quickEncode(t2)("adb".toList) === List(0, 0, 1, 0, 1))
      assert(quickEncode(frenchCode)("huffmanestcool".toList) === secret)
    }
  }


}
