package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }
  
  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   * 
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   * 
   *   val s1 = singletonSet(1)
   * 
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   * 
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   * 
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val emptySet: Set = { x => false }
  }

  trait TestSetsUnion extends TestSets {
    val s12 = union(s1, s2)
    val s13 = union(s1, s3)
    val s23 = union(s2, s3)
    val s123 = union(s1, s23)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   * 
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {
    
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
      assert(!contains(s1, 2), "Singleton")
    }
  }

  test("union contains all elements") {
    new TestSetsUnion {
      assert(contains(s12, 1))
      assert(contains(s12, 2))
      assert(!contains(s12, 3))

      assert(contains(s123, 1))
      assert(contains(s123, 2))
      assert(contains(s123, 3))
    }
  }

  test("intersect") {
    new TestSetsUnion {
      val i2 = intersect(s12, s23)
      assert(!contains(i2, 1))
      assert(contains(i2, 2))
      assert(!contains(i2, 3))

      val i23 = intersect(s123, s23)
      assert(!contains(i23, 1))
      assert(contains(i23, 2))
      assert(contains(i23, 3))
    }
  }

  test("diff") {
    new TestSetsUnion {
      val i1 = diff(s12, s23)
      assert(contains(i1, 1))
      assert(!contains(i1, 2))
      assert(!contains(i1, 3))

      val i3 = diff(s123, s12)
      assert(!contains(i3, 1))
      assert(!contains(i3, 2))
      assert(contains(i3, 3))
    }
  }

  test("filter") {
    new TestSetsUnion {
      val f23 = filter(s123, { _ > 1 })
      assert(!contains(f23, 1))
      assert(contains(f23, 2))
      assert(contains(f23, 3))

      val f1 = filter(s123, { _ < 2 })
      assert(contains(f1, 1))
      assert(!contains(f1, 2))
      assert(!contains(f1, 3))
    }
  }


  test("forall - On an empty set, everything holds for all members") {
    new TestSets {
      assert(forall(emptySet, { _ < 5 }))
      assert(forall(emptySet, { x => true }))
      assert(forall(emptySet, { x => false }))
    }
  }

  test("forall - all numbers under 4 are also under 5") {
    assert(forall({ _ < 4 }, { _ < 5 }))
  }

  test("forall - not all numbers above 4 are 2") {
    assert(!forall({ _ > 4 }, { _ == 2 }))
  }

  test("forall - not all numbers above 4 are 5") {
    assert(!forall({ _ > 4 }, { _ == 5 }))
  }

  test("forall - all odd numbers are not even") {
    assert(forall({ _ % 2 == 1 }, { _ % 2 != 0 }))
  }

  test("forall - all odd numbers, plus one, are even") {
    assert(forall({ _ % 2 == 1 }, { x => (x + 1) % 2 == 0 }))
  }


  test("exists - On an empty set, nothing exists") {
    new TestSets {
      assert(!exists(emptySet, { _ < 5 }))
      assert(!exists(emptySet, { x => true }))
      assert(!exists(emptySet, { x => false }))
    }
  }

  test("exists - there is a number under 4 that equals 2") {
    assert(exists({ _ < 4 }, { _ == 2 }))
  }

  test("exists - there is no number above 4 that equals 2") {
    assert(!exists({ _ > 4 }, { _ == 2 }))
  }

  test("exists - there is an even number that equals 2") {
    assert(exists({ _ % 2 == 0 }, { _ == 2 }))
  }

  test("exists - there is no even number that equals 11") {
    assert(!exists({ _ % 2 == 0 }, { _ == 11 }))
  }


  test("map - On an empty set, mapping whatever will always return an empty set") {
    new TestSets {
      val mEmpty = map(emptySet, { _ + 1 })
      assert(!contains(mEmpty, 1))
      assert(!contains(mEmpty, 2))
      assert(!contains(mEmpty, 3))
      assert(!contains(mEmpty, 4))
    }
  }

  test("map - +1") {
    new TestSetsUnion {
      val m34 = map(s23, { _ + 1 })
      assert(!contains(m34, 1))
      assert(!contains(m34, 2))
      assert(contains(m34, 3))
      assert(contains(m34, 4))
    }
  }

  test("map - -1") {
    new TestSetsUnion {
      val m12 = map(s23, { _ - 1 })
      assert(contains(m12, 1))
      assert(contains(m12, 2))
      assert(!contains(m12, 3))
      assert(!contains(m12, 4))
    }
  }

}
