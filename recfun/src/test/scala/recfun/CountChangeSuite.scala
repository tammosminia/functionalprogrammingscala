package recfun

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CountChangeSuite extends FunSuite {
  import Main.countChange

  test("countChange: 1") {
    assert(countChange(1, List(1)) === 1)
  }

  test("countChange: 3*1") {
    assert(countChange(3, List(1)) === 1)
  }

  test("countChange: overbodige 2") {
    assert(countChange(1, List(2,1)) === 1)
  }

  test("countChange: overbodige 2 laatst") {
    assert(countChange(1, List(1,2)) === 1)
  }

  test("countChange: [2] en [1,1]") {
    assert(countChange(2, List(1,2)) === 2)
  }

  test("countChange: [2,2], [2,1,1] en [1,1,1,1]") {
    assert(countChange(4, List(1,2)) === 3)
  }

  test("countChange: [3,1], [2,2], [2,1,1] en [1,1,1,1]") {
    assert(countChange(4, List(1,2,3)) === 4)
  }

  test("countChange: impossible") {
    assert(countChange(4,List(5)) === 0)
  }

  test("countChange: possible") {
    assert(countChange(10, List(1, 5, 10, 20)) === 4)
  }

  test("countChange: simpler CHF") {
    assert(countChange(100, List(5,10,20,50,100,200,500)) === 50)
  }

  test("countChange: simpler no pennies") {
    assert(countChange(101, List(5,10,20,50,100,200,500)) === 0)
  }

  test("countChange: sorted CHF") {
    assert(countChange(300, List(5,10,20,50,100,200,500)) === 1022)
  }

  test("countChange: no pennies") {
    assert(countChange(301, List(5,10,20,50,100,200,500)) === 0)
  }

  test("countChange: unsorted CHF") {
    assert(countChange(300, List(500,5,50,100,20,200,10)) === 1022)
  }
}
