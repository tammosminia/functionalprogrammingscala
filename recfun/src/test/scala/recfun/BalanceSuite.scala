package recfun

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BalanceSuite extends FunSuite {
  import Main._

  test("lege string") {
    assert(balance("".toList))
  }

  //test voor een hulpfunctie
// test("lege string, toch nog een open haakje") {
//    assert(!balanceHelp("".toList, 1))
//  }

  test("a") {
    assert(balance("a".toList))
  }

  test("(") {
    assert(!balance("(".toList))
  }

  test(")") {
    assert(!balance(")".toList))
  }

  test("()") {
    assert(balance("()".toList))
  }

  test(")(") {
    assert(!balance(")(".toList))
  }

  test("balance: '(if (zero? x) max (/ 1 x))' is balanced") {
    assert(balance("(if (zero? x) max (/ 1 x))".toList))
  }

  test("balance: 'I told him ...' is balanced") {
    assert(balance("I told him (that it's not (yet) done).\n(But he wasn't listening)".toList))
  }

  test("balance: ':-)' is unbalanced") {
    assert(!balance(":-)".toList))
  }

  test("balance: counting is not enough") {
    assert(!balance("())(".toList))
  }
}
