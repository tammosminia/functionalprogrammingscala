package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = (c, r) match {
    case (0, _) => 1
    case (column: Int, row: Int) if (column < 0 || column > row) => 0
    case (column, row) => pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = balance1(chars, 0)

  def balance1(chars: List[Char], openParentheses: Int): Boolean = chars match {
    case Nil => openParentheses == 0
    case '(' :: tail => balance1(tail, openParentheses + 1)
    case ')' :: tail if (openParentheses == 0) => false
    case ')' :: tail => balance1(tail, openParentheses - 1)
    case head :: tail => balance1(tail, openParentheses)
  }

  /**
   * Exercise 3
   */
  def countChange(moneyTotal: Int, coins: List[Int]): Int = {
    if (moneyTotal < 0) {
      0
    } else if (coins.isEmpty) {
      if (moneyTotal == 0) {
        1
      } else {
        0
      }
    } else {
      countChange(moneyTotal - coins.head, coins) + countChange(moneyTotal, coins.tail)
    }
  }

//Te moeilijke manier, voordat ik ontdekte dat het allemaal makkelijker kon
//  def countChange(money: Int, coins: List[Int]): Int = changes(money, coins).map { _.sorted }.toSet.size
//
//  def changes(money: Int, coins: List[Int]): List[List[Int]] = {
//    if (money == 0) return List(Nil)
//    val usableCoins = coins.filter { _ <= money }
//    usableCoins.flatMap { coin =>
//      changes(money - coin, coins).map { coin :: _ }
//    }
//  }
}
